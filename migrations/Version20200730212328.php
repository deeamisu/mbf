<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200730212328 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE partial_result (id INT AUTO_INCREMENT NOT NULL, competitor_id INT NOT NULL, judge_id INT NOT NULL, grade DOUBLE PRECISION NOT NULL, cookie VARCHAR(255) DEFAULT NULL, INDEX IDX_F8A0010478A5D405 (competitor_id), INDEX IDX_F8A00104B7D66194 (judge_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE partial_result ADD CONSTRAINT FK_F8A0010478A5D405 FOREIGN KEY (competitor_id) REFERENCES competitor (id)');
        $this->addSql('ALTER TABLE partial_result ADD CONSTRAINT FK_F8A00104B7D66194 FOREIGN KEY (judge_id) REFERENCES judge (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE partial_result');
    }
}
