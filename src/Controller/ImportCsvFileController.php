<?php

namespace App\Controller;

use App\Entity\ImportCsvFile;
use App\Form\ImportCsvFileType;
use App\Repository\ImportCsvFileRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @IsGranted("ROLE_SCRUTINEER")
 * @Route("/scrut/import/csv/file")
 */
class ImportCsvFileController extends AbstractController
{
    /**
     * @Route("/", name="import_csv_file_index", methods={"GET"})
     */
    public function index(ImportCsvFileRepository $importCsvFileRepository): Response
    {
        return $this->render('import_csv_file/index.html.twig', [
            'import_csv_files' => $importCsvFileRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="import_csv_file_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $importCsvFile = new ImportCsvFile();
        $form = $this->createForm(ImportCsvFileType::class, $importCsvFile);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $csvFile = $form->get('filename')->getData();

            if ($csvFile)
            {

                try {
                    $csvFile->move(
                        $this->getParameter('inscrieri_directory'),
                        $csvFile->getClientOriginalName()
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

            }


            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($importCsvFile);
            $entityManager->flush();

            return $this->redirectToRoute('scrutineer_import');
        }

        return $this->render('import_csv_file/new.html.twig', [
            'import_csv_file' => $importCsvFile,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="import_csv_file_show", methods={"GET"})
     */
    public function show(ImportCsvFile $importCsvFile): Response
    {
        return $this->render('import_csv_file/show.html.twig', [
            'import_csv_file' => $importCsvFile,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="import_csv_file_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ImportCsvFile $importCsvFile): Response
    {
        $form = $this->createForm(ImportCsvFileType::class, $importCsvFile);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('import_csv_file_index');
        }

        return $this->render('import_csv_file/edit.html.twig', [
            'import_csv_file' => $importCsvFile,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="import_csv_file_delete", methods={"DELETE"})
     */
    public function delete(Request $request, ImportCsvFile $importCsvFile): Response
    {
        if ($this->isCsrfTokenValid('delete'.$importCsvFile->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($importCsvFile);
            $entityManager->flush();
        }

        return $this->redirectToRoute('import_csv_file_index');
    }
}
