<?php
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 7/20/2020
 * Time: 4:05 AM
 */

namespace App\Service;

use App\Repository\EventRepository;
use App\Repository\PresidentRepository;
use App\Repository\ScrutineerRepository;
use Doctrine\ORM\EntityManagerInterface;


class SetEventDetails
{
    private $eventRepository;

    private $ceremonymasterRepository;

    private $chairpersonRepository;

    private $entityManager;

    /**
     * SetEventDetails constructor.
     * @param $eventRepository
     * @param $ceremonymasterRepository
     * @param $chairpersonRepository
     * @param $entityManager
     */
    public function __construct(EventRepository $eventRepository, ScrutineerRepository $ceremonymasterRepository,
                                PresidentRepository $chairpersonRepository, EntityManagerInterface $entityManager)
    {
        $this->eventRepository = $eventRepository;
        $this->ceremonymasterRepository = $ceremonymasterRepository;
        $this->chairpersonRepository = $chairpersonRepository;
        $this->entityManager=$entityManager;
    }

    public function setEventCeremonyMaster($eventId,$ceremonyMasterId)
    {
        $event=$this->eventRepository->find($eventId);
        $event->setScrutineer($this->ceremonymasterRepository->find($ceremonyMasterId));
        $this->entityManager->flush();
    }

    public function setEventChairPerson($eventId,$chairPersonId)
    {
        $event=$this->eventRepository->find($eventId);
        $event->setPresident($this->chairpersonRepository->find($chairPersonId));
        $this->entityManager->flush();
    }
}