<?php
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 7/30/2020
 * Time: 1:03 AM
 */

namespace App\Service;


use App\Entity\Competition;
use App\Entity\Competitor;
use App\Entity\Criteria;
use App\Entity\Event;
use App\Entity\Grade;
use App\Entity\Judge;
use App\Entity\PartialResult;


class JudgeMenu extends Base
{
    //private $entityManager;

    /**
     * JudgeMenu constructor.
     * @param $entityManager
     */
    /**public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }*/

    public function getCompetitions($judgeid,$eventid)
    {
        $event=$this->entityManager->getRepository(Event::class)->find($eventid);
        $judge=$this->entityManager->getRepository(Judge::class)->find($judgeid);

        /**
         * @var $eventCompetitions Competition[]
         */
        $eventCompetitions=$event->getCompetitions();
        /**
         * @var $judgeCompetitions Competition[]
         */
        $judgeCompetitions=$judge->getCompetitions();

        $competitions=[];
        foreach ($eventCompetitions as $eventCompetition):
            foreach ($judgeCompetitions as $judgeCompetition)
            {
                if ($eventCompetition->getId() == $judgeCompetition->getId()):
                    $competitions[]=$judgeCompetition;
                endif;
            }
        endforeach;

        return $competitions;
    }

    public function getGradeRequirements($competitionid, $judgeid, $athletid)
    {
        $credentials=[];

        $competition=$this->entityManager->getRepository(Competition::class)->find($competitionid);

        $credentials['criterias']=$competition->getCriteria();

        return $credentials;
    }

    public function compileGrades($marks,$judgeid,$athletid)
    {

        $judge=$this->entityManager->getRepository(Judge::class)->find($judgeid);
        $competitor=$this->entityManager->getRepository(Competitor::class)->find($athletid);

        foreach ($marks as $mark):
            $grade=new Grade();
            $criteria=$this->entityManager->getRepository(Criteria::class)->find($mark['criteriaid']);
            $grade->setCriteria($criteria)
                ->setValue($mark['value'])
                ->setCompetitor($competitor)
                ->setJudge($judge)
            ;
            $this->entityManager->persist($grade);
        endforeach;

        $this->entityManager->flush();

    }

    /**
     * @param $competitors Competitor[]
     */
    public function getCompetitors($competitors,$judgeid)
    {
        $judge=$this->entityManager->getRepository(Judge::class)->find($judgeid);

        foreach ($competitors as $key=>$competitor):
            $grades=$competitor->getGrades();
            foreach ($grades as $grade)
            {
                if ($grade->getJudge() == $judge):
                    unset($competitors[$key]);
                    break;
                endif;
            }
        endforeach;

        return $competitors;
    }

    /**
     * @param $competitors Competitor[]
     */
    public function calculate($competitors,$judgeid,$competitionid)
    {
        if (count($competitors) == 0):
            $competition=$this->entityManager->getRepository(Competition::class)->find($competitionid);
            $judge=$this->entityManager->getRepository(Judge::class)->find($judgeid);

            $query=$this->entityManager->createQuery(
                'SELECT c
                FROM App\Entity\Competitor c
                WHERE c.competition = :competition'
            )->setParameter('competition',$competition);
            $athlets=$query->getResult();

            foreach ($athlets as $athlet):

                if ($this->check($athlet,$judge)):

                $query=$this->entityManager->createQuery(
                    'SELECT p 
                    FROM App\Entity\Grade p
                    WHERE p.judge = :judge
                    AND p.competitor = :competitor'
                )->setParameters(['judge'=>$judge,'competitor'=>$athlet]);
                $grades=$query->getResult();

                $partialvalue=0;

                /**
                 * @var $grades Grade[]
                 */
                foreach ($grades as $grade)
                {
                    $criteria=$grade->getCriteria();
                    $criteriaValue=$criteria->getProcent();
                    $valueLength=ceil(log10($criteriaValue));

                    if ($valueLength == 1){
                        $valueLength++;
                    }

                    $partialvalue+=$grade->getValue()*$criteriaValue/pow(10,$valueLength);
                }

                $parialResult=new PartialResult();
                $parialResult->setCompetitor($athlet)
                            ->setJudge($judge)
                            ->setGrade($partialvalue)
                            ->setCookie('resolved');

                $this->entityManager->persist($parialResult);
                $this->entityManager->flush();

                endif;

            endforeach;
        endif;
    }

    public function check($athlet,$judge)
    {
        $partialResult=$this->entityManager->getRepository(PartialResult::class)
            ->findBy(['competitor'=>$athlet,'judge'=>$judge]);

        if (count($partialResult) > 0):
            if ($partialResult[0]->getCookie() == 'resolved')
            {
                return false;
            }
        endif;
        return true;
    }
}