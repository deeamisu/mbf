<?php

namespace App\Service;

use App\Repository\PresidentRepository;
use App\Repository\ScrutineerRepository;


/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 7/20/2020
 * Time: 2:45 AM
 */
class GetOficials
{
    private $chairpersonRepository;
    private $ceremonymasterRepository;

    /**
     * GetOficials constructor.
     * @param $chairpersonRepository
     * @param $ceremonymasterRepository
     */
    public function __construct(PresidentRepository $chairPersonRepository, ScrutineerRepository $ceremonyMasterRepository)
    {
        $this->chairpersonRepository = $chairPersonRepository;
        $this->ceremonymasterRepository = $ceremonyMasterRepository;
    }


    public function getChairPersons()
    {
        $oficials=$this->chairpersonRepository->findAll();
        return $oficials;
    }
    public function getCeremonyMasters()
    {
        $oficials=$this->ceremonymasterRepository->findAll();
        return $oficials;
    }

}