<?php

namespace App\Entity;

use App\Repository\PartialResultRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PartialResultRepository::class)
 */
class PartialResult
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Competitor::class, inversedBy="partialResults")
     * @ORM\JoinColumn(nullable=false)
     */
    private $competitor;

    /**
     * @ORM\Column(type="float")
     */
    private $grade;

    /**
     * @ORM\ManyToOne(targetEntity=Judge::class, inversedBy="partialResults")
     * @ORM\JoinColumn(nullable=false)
     */
    private $judge;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cookie;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCompetitor(): ?Competitor
    {
        return $this->competitor;
    }

    public function setCompetitor(?Competitor $competitor): self
    {
        $this->competitor = $competitor;

        return $this;
    }

    public function getGrade(): ?float
    {
        return $this->grade;
    }

    public function setGrade(?float $grade): self
    {
        $this->grade = $grade;

        return $this;
    }

    public function getJudge(): ?Judge
    {
        return $this->judge;
    }

    public function setJudge(?Judge $judge): self
    {
        $this->judge = $judge;

        return $this;
    }

    public function getCookie(): ?string
    {
        return $this->cookie;
    }

    public function setCookie(?string $cookie): self
    {
        $this->cookie = $cookie;

        return $this;
    }
}
