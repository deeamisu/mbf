<?php
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 8/3/2020
 * Time: 12:41 AM
 */

namespace App\Service;


use App\Entity\Competition;
use App\Entity\Competitor;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Twig\Environment;
use Symfony\Component\HttpFoundation\Response;
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;


class Results extends Base
{
    private $twig;

    //private $entityManager;

    private $parameterBag;

    private $mailer;


    /**
     * Results constructor.
     * @param $twig
     * @param $entityManager
     * @param $parameterBag
     * @param $mailer
     */
    public function __construct(Environment $twig,EntityManagerInterface $entityManager, ParameterBagInterface $parameterBag, MailerInterface $mailer)
    {
        parent::__construct($entityManager, $parameterBag);

        $this->twig = $twig;
        //$this->entityManager = $entityManager;
        $this->parameterBag = $parameterBag;
        $this->mailer = $mailer;
    }


    public function createPdfResultsFiles()
    {
        $competitors=$this->entityManager->getRepository(Competitor::class)->findAll();
        foreach ($competitors as $competitor) {
            /**
             * @var $competition Competition
             */
            $competition = $competitor->getCompetition();
            $grades = $competitor->getGrades();
            $finalresult = $competitor->getResult();
            $criterias = $competition->getCriteria();
            $judges = $competition->getJudge();
            $filename=$competitor->getId().'.pdf';

            /**return $response= new Response($this->twig->render('scrutineer_menu/pdfCompetitorResults.html.twig',[
             * 'competitor' => $competitor,
             * 'competition' => $competition,
             * 'grades' => $grades,
             * 'result' => $finalresult,
             * 'criterias' => $criterias,
             * 'judges' => $judges
             * ]));
             */

            // Configure Dompdf according to your needs
            $pdfOptions = new Options();
            $pdfOptions->set('defaultFont', 'Arial');

            // Instantiate Dompdf with our options
            $dompdf = new Dompdf($pdfOptions);

            // Retrieve the HTML generated in our twig file
            $html = $this->twig->render('scrutineer_menu/pdfCompetitorResults.html.twig', [
                'competitor' => $competitor,
                'competition' => $competition,
                'grades' => $grades,
                'result' => $finalresult,
                'criterias' => $criterias,
                'judges' => $judges
            ]);

            // Load HTML to Dompdf
            $dompdf->loadHtml($html);

            // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
            $dompdf->setPaper('A4', 'landscape');

            // Render the HTML as PDF
            $dompdf->render();

            // Store PDF Binary Data
            $output = $dompdf->output();

            // In this case, we want to write the file in the public directory
            $raportDirectory = $this->parameterBag->get('kernel.project_dir') . '/public/raports';
            // e.g /var/www/project/public/mypdf.pdf
            $pdfFilepath = $raportDirectory . '/'.$filename;

            // Write file to the desired path
            file_put_contents($pdfFilepath, $output);
        }
    }

    public function sendEmail()
    {

        //$competitors=$this->entityManager->getRepository(Competitor::class)->findAll();
        /**
         * @var $competitors Competitor[]
         */
        $competitors=self::getAllCompetitors();

        foreach ($competitors as $competitor) {
            $pdfFilePath = $this->parameterBag->get('kernel.project_dir').'/public/raports/'.$competitor->getId().'.pdf';
            /**
             * @var $competition Competition
             */
            $competition=$competitor->getCompetition();

            $email = (new Email())
                ->from('misudeea2009@gmail.com')
                ->to($competitor->getEmail())
                //->cc('cc@example.com')
                //->bcc('bcc@example.com')
                //->replyTo('fabien@example.com')
                //->priority(Email::PRIORITY_HIGH)
                ->subject('Rezultate '.$competition->getName())
                ->text('Draga '.$competitor.', in fisierul alaturat va trimitem rezultatele obtinute la '.$competition->getEvent().
                    '. Cu respect, '.$competition->getEvent()->getPresident().' si '. $competition->getEvent()->getScrutineer())
                //->html('<p>See Twig integration for better HTML integration!</p>');
                ->attachFromPath($pdfFilePath);

            //$email->getHeaders()->addTextHeader('X-Transport', 'alternative');
            $this->mailer->send($email);
        }
    }
}