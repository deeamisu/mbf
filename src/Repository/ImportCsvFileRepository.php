<?php

namespace App\Repository;

use App\Entity\ImportCsvFile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ImportCsvFile|null find($id, $lockMode = null, $lockVersion = null)
 * @method ImportCsvFile|null findOneBy(array $criteria, array $orderBy = null)
 * @method ImportCsvFile[]    findAll()
 * @method ImportCsvFile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ImportCsvFileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ImportCsvFile::class);
    }

    // /**
    //  * @return ImportCsvFile[] Returns an array of ImportCsvFile objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ImportCsvFile
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
