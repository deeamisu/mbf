<?php

namespace App\Controller;

use App\Entity\Competitor;
use App\Form\CompetitorType;
use App\Repository\CompetitorRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @IsGranted("ROLE_SCRUTINEER")
 * @Route("scrut/competitor")
 */
class CompetitorController extends AbstractController
{
    /**
     * @Route("/", name="competitor_index", methods={"GET"})
     */
    public function index(CompetitorRepository $competitorRepository): Response
    {
        return $this->render('competitor/index.html.twig', [
            'competitors' => $competitorRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="competitor_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $competitor = new Competitor();
        $form = $this->createForm(CompetitorType::class, $competitor);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($competitor);
            $entityManager->flush();

            return $this->redirectToRoute('competitor_index');
        }

        return $this->render('competitor/new.html.twig', [
            'competitor' => $competitor,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="competitor_show", methods={"GET"})
     */
    public function show(Competitor $competitor): Response
    {
        return $this->render('competitor/show.html.twig', [
            'competitor' => $competitor,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="competitor_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Competitor $competitor): Response
    {
        $form = $this->createForm(CompetitorType::class, $competitor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('competitor_index');
        }

        return $this->render('competitor/edit.html.twig', [
            'competitor' => $competitor,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="competitor_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Competitor $competitor): Response
    {
        if ($this->isCsrfTokenValid('delete'.$competitor->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($competitor);
            $entityManager->flush();
        }

        return $this->redirectToRoute('competitor_index');
    }
}
