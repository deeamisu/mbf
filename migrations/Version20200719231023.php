<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200719231023 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE event ADD ceremony_master_id_id INT DEFAULT NULL, ADD chair_person_id_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA7715CD8AC FOREIGN KEY (ceremony_master_id_id) REFERENCES ceremony_master (id)');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA7D19A5E25 FOREIGN KEY (chair_person_id_id) REFERENCES chair_person (id)');
        $this->addSql('CREATE INDEX IDX_3BAE0AA7715CD8AC ON event (ceremony_master_id_id)');
        $this->addSql('CREATE INDEX IDX_3BAE0AA7D19A5E25 ON event (chair_person_id_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE event DROP FOREIGN KEY FK_3BAE0AA7715CD8AC');
        $this->addSql('ALTER TABLE event DROP FOREIGN KEY FK_3BAE0AA7D19A5E25');
        $this->addSql('DROP INDEX IDX_3BAE0AA7715CD8AC ON event');
        $this->addSql('DROP INDEX IDX_3BAE0AA7D19A5E25 ON event');
        $this->addSql('ALTER TABLE event DROP ceremony_master_id_id, DROP chair_person_id_id');
    }
}
