<?php

namespace App\Controller;

use App\Entity\Competition;
use App\Entity\Criteria;
use App\Entity\Event;
use App\Entity\Judge;
use App\Entity\President;
use App\Service\PresidentMenu;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


/**
 * @IsGranted("ROLE_PRESIDENT")
 * @Route{"/presid")
 */
class PresidentMenuController extends AbstractController
{
    /**
     * @Route("/president/menu", name="president_menu")
     */
    public function index()
    {
        return $this->render('president_menu/index.html.twig', [
            'controller_name' => 'PresidentMenuController',
        ]);
    }

    /**
     * @Route("/president/events/{id}", name="president_get_events", methods={"GET"})
     */
    public function getPresidentEvents($id , EntityManagerInterface $entityManager)
    {

        $chairperson=$entityManager->getRepository(President::class)->find($id);
        return $this->render("president_menu/events.html.twig",['events'=>$chairperson->getEvents(),
                                        'president'=>$chairperson]);
    }

    /**
     * @Route("/president/events/competitions/{pid}/{eid}", name="president_get_competitions_event", methods={"GET"})
     */
    public function getPresidentEventCompetitions($pid, $eid ,EntityManagerInterface $entityManager)
    {
        $chairperson=$entityManager->getRepository(President::class)->find($pid);
        $event=$entityManager->getRepository(Event::class)->find($eid);
        $competitions=$event->getCompetitions();
        return $this->render('president_menu/eventcompetitions.html.twig',
            ['president'=>$chairperson, 'event'=>$event, 'competitions'=>$competitions]);
    }

    /**
     * @Route("/president/competition/menu/{pid}/{cid}", name="president_competition_menu", methods={"GET"})
     */
    public function presidentCompetitionMenu($pid,$cid, EntityManagerInterface $entityManager)
    {
        $chairperson=$entityManager->getRepository(President::class)->find($pid);
        $competition=$entityManager->getRepository(Competition::class)->find($cid);
        return $this->render('president_menu/presidentCompetitionMenu.html.twig',
            ['president'=>$chairperson, 'competition'=>$competition]);
    }

    /**
     * @Route("/president/competition/menu/judge/{pid}/{cid}", name="president_comp_judges", methods={"GET"})
     */
    public function showCompetitionJudges($pid, $cid, PresidentMenu $presidentMenu)
    {
        $credentials=$presidentMenu->getCredentialsCompetitionPresident($pid,$cid,true);

        return $this->render('president_menu/showCompetitionJudges.html.twig',
                ['president' => $credentials['president'],
                'competition' => $credentials['competition'],
                'judges' => $credentials['judges'],
                'addbleJudges' => $credentials['addbleJudges']]);
    }

    /**
     * @Route("/president/competition/menu/judge/add/{pid}/{cid}/{judgeid}", name="president_addJudge_Comp", methods={"GET"})
     */
    public function addJudgeToCompetition($pid,$cid,$judgeid, PresidentMenu $presidentMenu, EntityManagerInterface $entityManager)
    {
        $competition=$entityManager->getRepository(Competition::class)->find($cid);
        $judge=$entityManager->getRepository(Judge::class)->find($judgeid);
        $competition->addJudge($judge);
        $entityManager->flush();

        $credentials=$presidentMenu->getCredentialsCompetitionPresident($pid,$cid,true);

        return $this->render('president_menu/showCompetitionJudges.html.twig',
            ['president' => $credentials['president'],
                'competition' => $credentials['competition'],
                'judges' => $credentials['judges'],
                'addbleJudges' => $credentials['addbleJudges']]);
    }

    /**
     * @Route("/president/competition/menu/judge/remove/{pid}/{cid}/{judgeid}", name="president_removeJudge_Comp", methods={"GET"})
     */
    public function removeJudgeFromCompetition($pid,$cid,$judgeid, PresidentMenu $presidentMenu, EntityManagerInterface $entityManager)
    {
        $competition=$entityManager->getRepository(Competition::class)->find($cid);
        $judge=$entityManager->getRepository(Judge::class)->find($judgeid);
        $competition->removeJudge($judge);
        $entityManager->flush();

        $credentials=$presidentMenu->getCredentialsCompetitionPresident($pid,$cid,true);

        return $this->render('president_menu/showCompetitionJudges.html.twig',
            ['president' => $credentials['president'],
                'competition' => $credentials['competition'],
                'judges' => $credentials['judges'],
                'addbleJudges' => $credentials['addbleJudges']]);
    }

    /**
     * @Route("/president/competition/menu/criteria/{pid}/{cid}", name="president_comp_criterias", methods={"GET"})
     */
    public function showCompetitionCriterias($pid, $cid, PresidentMenu $presidentMenu)
    {
        $credentials=$presidentMenu->getCredentialsCompetitionPresident($pid,$cid,false,true);

        return $this->render('president_menu/showCompetitionsCriterias.html.twig',
            ['president' => $credentials['president'],
                'competition' => $credentials['competition'],
                'criterias' => $credentials['criterias'],
                'addbleCriterias' => $credentials['addbleCriterias']]);
    }

    /**
     * @Route("/president/competition/menu/criteria/add/{pid}/{cid}/{critid}", name="president_addCriteria_Comp", methods={"GET"})
     */
    public function addCriteriaToCompetition($pid,$cid,$critid, PresidentMenu $presidentMenu, EntityManagerInterface $entityManager)
    {
        $competition=$entityManager->getRepository(Competition::class)->find($cid);
        $criteria=$entityManager->getRepository(Criteria::class)->find($critid);
        $competition->addCriterion($criteria);
        $entityManager->flush();

        $credentials=$presidentMenu->getCredentialsCompetitionPresident($pid,$cid,false,true);

        return $this->render('president_menu/showCompetitionsCriterias.html.twig',
            ['president' => $credentials['president'],
                'competition' => $credentials['competition'],
                'criterias' => $credentials['criterias'],
                'addbleCriterias' => $credentials['addbleCriterias']]);
    }

    /**
     * @Route("/president/competition/menu/criteria/remove/{pid}/{cid}/{critid}", name="president_removeCriteria_Comp", methods={"GET"})
     */
    public function removeCriteriaFromCompetition($pid,$cid,$critid, PresidentMenu $presidentMenu, EntityManagerInterface $entityManager)
    {
        $competition=$entityManager->getRepository(Competition::class)->find($cid);
        $criteria=$entityManager->getRepository(Criteria::class)->find($critid);
        $competition->removeCriterion($criteria);
        $entityManager->flush();

        $credentials=$presidentMenu->getCredentialsCompetitionPresident($pid,$cid,false,true);

        return $this->render('president_menu/showCompetitionsCriterias.html.twig',
            ['president' => $credentials['president'],
                'competition' => $credentials['competition'],
                'criterias' => $credentials['criterias'],
                'addbleCriterias' => $credentials['addbleCriterias']]);
    }

}
