<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class InitController extends AbstractController
{
    /**
     * @Route("/init", name="init")
     */
    public function index()
    {
        return $this->render('init/index.html.twig', [
            'controller_name' => 'InitController',
        ]);
    }
}
