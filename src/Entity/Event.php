<?php

namespace App\Entity;

use App\Repository\EventRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EventRepository::class)
 */
class Event
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $location;

    /**
     * @ORM\OneToMany(targetEntity=Competition::class, mappedBy="event", orphanRemoval=true)
     */
    private $competitions;

    /**
     * @ORM\ManyToOne(targetEntity=President::class, inversedBy="events")
     */
    private $President;

    /**
     * @ORM\ManyToOne(targetEntity=Scrutineer::class, inversedBy="events")
     */
    private $Scrutineer;

    /**
     * @ORM\OneToMany(targetEntity=Criteria::class, mappedBy="event", orphanRemoval=true)
     */
    private $criterias;

    public function __construct()
    {
        $this->competitions = new ArrayCollection();
        $this->criterias = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): self
    {
        $this->location = $location;

        return $this;
    }

    /**
     * @return Collection|Competition[]
     */
    public function getCompetitions(): Collection
    {
        return $this->competitions;
    }

    public function addCompetition(Competition $competition): self
    {
        if (!$this->competitions->contains($competition)) {
            $this->competitions[] = $competition;
            $competition->setEvent($this);
        }

        return $this;
    }

    public function removeCompetition(Competition $competition): self
    {
        if ($this->competitions->contains($competition)) {
            $this->competitions->removeElement($competition);
            // set the owning side to null (unless already changed)
            if ($competition->getEvent() === $this) {
                $competition->setEvent(null);
            }
        }

        return $this;
    }

    function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->getName();
    }

    public function getPresident(): ?President
    {
        return $this->President;
    }

    public function setPresident(?President $President): self
    {
        $this->President = $President;

        return $this;
    }

    public function getScrutineer(): ?Scrutineer
    {
        return $this->Scrutineer;
    }

    public function setScrutineer(?Scrutineer $Scrutineer): self
    {
        $this->Scrutineer = $Scrutineer;

        return $this;
    }

    /**
     * @return Collection|Criteria[]
     */
    public function getCriterias(): Collection
    {
        return $this->criterias;
    }

    public function addCriteria(Criteria $criteria): self
    {
        if (!$this->criterias->contains($criteria)) {
            $this->criterias[] = $criteria;
            $criteria->setEvent($this);
        }

        return $this;
    }

    public function removeCriteria(Criteria $criteria): self
    {
        if ($this->criterias->contains($criteria)) {
            $this->criterias->removeElement($criteria);
            // set the owning side to null (unless already changed)
            if ($criteria->getEvent() === $this) {
                $criteria->setEvent(null);
            }
        }

        return $this;
    }

}
