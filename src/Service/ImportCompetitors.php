<?php
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 7/23/2020
 * Time: 2:31 AM
 */

namespace App\Service;


use App\Entity\Competition;
use App\Entity\Competitor;
use App\Entity\Grade;
use App\Entity\PartialResult;
use App\Repository\CompetitorRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;

class ImportCompetitors extends Base
{
    //private $entityManager;

    private $competitorRepository;

    /**
     * ImportCompetitors constructor.
     * @param $entityManager
     */
    public function __construct( CompetitorRepository $competitorRepository, EntityManagerInterface  $entityManager, ParameterBagInterface $parameterBag)
    {
        //$this->entityManager = $entityManager;
        parent::__construct($entityManager, $parameterBag);
        $this->competitorRepository=$competitorRepository;
    }


    public function importCompetitorsFromCsv()
    {
        $competitors= array_map('str_getcsv', file('uploads/inscrieri/inscrieri.csv'));
        $keys=array_shift($competitors);

        $arrangedCompetitors=[];

        foreach ($competitors as $competitor):
            $arrangedCompetitors1=[];
            foreach ($competitor as $key=>$item)
            {
                $arrangedCompetitors1[$keys[$key]]= $item;
            }
            $arrangedCompetitors[]=$arrangedCompetitors1;
        endforeach;

        //var_dump($arrangedCompetitors);
        //die();

        foreach ($arrangedCompetitors as $key=>$arrangedCompetitor):
            $comp=new Competitor();
            $comp->setName1($arrangedCompetitor['name1']);
            $comp->setName2($arrangedCompetitor['name2']);
            $comp->setCode($arrangedCompetitor['code']);
            $comp->setEmail($arrangedCompetitor['email']);
            $this->entityManager->persist($comp);

        endforeach;

        $this->entityManager->flush();

        $this->assignCompetitorsToCompetitions();

        return $this->competitorRepository->findAll();
    }

    public function clearCompetitors()
    {
        $grades=$this->entityManager->getRepository(Grade::class)->findAll();
        foreach ($grades as $grade):
            $this->entityManager->remove($grade);
        endforeach;
        $this->entityManager->flush();

        $presults=$this->entityManager->getRepository(PartialResult::class)->findAll();
        foreach ($presults as $presult):
            $this->entityManager->remove($presult);
        endforeach;
        $this->entityManager->flush();

        $competitors=$this->competitorRepository->findAll();
        foreach ($competitors as $competitor):
            $this->entityManager->remove($competitor);
        endforeach;
        $this->entityManager->flush();

        $filesystem= new Filesystem();
        $filesystem->remove(['','','uploads/inscrieri/inscrieri.csv']);
    }

    public function assignCompetitorsToCompetitions()
    {
        $competitiors=$this->entityManager->getRepository(Competitor::class)->findAll();
        $competitions=$this->entityManager->getRepository(Competition::class)->findAll();
        foreach ($competitions as $competition)
        {
            foreach ($competitiors as $competitor):
                if ($competition->getCode() == $competitor->getCode()):
                    //$competitor->setCompetition($competition);
                    $competition->addCompetitor($competitor);
                endif;
            endforeach;
        }
        $this->entityManager->flush();
    }
}