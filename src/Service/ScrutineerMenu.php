<?php
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 7/25/2020
 * Time: 1:17 AM
 */

namespace App\Service;


use App\Entity\Competition;
use App\Entity\Result;
use App\Entity\Scrutineer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;


class ScrutineerMenu extends Base
{
    //private $entityManager;

    /**
     * ScrutineerMenu constructor.
     * @param $entityManager
     */
    /**public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }*/


    public function getEvents($id)
    {
        $scrutineer=$this->entityManager->getRepository(Scrutineer::class)->find($id);
        $events=$scrutineer->getEvents();
        return $events;
    }


    public function result($compid)
    {
        $competition=$this->entityManager->getRepository(Competition::class)->find($compid);
        $athlets=$competition->getCompetitors();

        $finalResultsCheck=$competition->getResults();

        if (count($finalResultsCheck) == 0):
            foreach ($athlets as $athlet):
                $partialResults=$athlet->getPartialResults();
                $result=0;
                foreach ($partialResults as $partialResult)
                {
                    $result+=$partialResult->getGrade();
                }

                $finalResult= new Result();
                $finalResult->setCompetitor($athlet)
                            ->setValue($result)
                            ->setCompetition($competition)
                            ->setCookie('resolved');
                $this->entityManager->persist($finalResult);
                $this->entityManager->flush();
            endforeach;
        endif;

        $query=$this->entityManager->createQuery(
            'SELECT r
                FROM App\Entity\Result r 
                WHERE r.competition = :competition
                ORDER BY r.value DESC '
        )->setParameter('competition',$competition);
        $results=$query->getResult();

        $place=0;
        foreach ($results as $result)
        {
            $place++;
            $result->setPlace($place);
        }
        $this->entityManager->flush();

        return $results;
    }

    public function setRegisterFile()
    {
        $filepath=self::getPublicDir().'/downloads/inscrieri.csv';

        $fileContent='name1,name2,code,email';

        $file=new Filesystem();
        $file->dumpFile($filepath,$fileContent);

        return $filepath;
    }
}