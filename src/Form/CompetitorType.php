<?php

namespace App\Form;

use App\Entity\Competition;
use App\Entity\Competitor;
use App\Entity\Scrutineer;
use App\Repository\ScrutineerRepository;
use App\Repository\CompetitionRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Security\Core\Security;



class CompetitorType extends AbstractType
{

    private $security;

    /**
     * CompetitorType constructor.
     * @param $security
     */
    public function __construct(Security $security)
    {
        $this->security = $security;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name1')
            ->add('name2')
            //->add('code')
            ->add('email')
            //->add('competition')
        ;

        // grab the user, do a quick sanity check that one exists
        $user = $this->security->getUser();
        if (!$user) {
            throw new \LogicException(
                'The FriendMessageFormType cannot be used without an authenticated user!'
            );
        }

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($user) {

            $form = $event->getForm();

            $formOptions = [
                'class' => Competition::class,
                'choice_label' => 'name',
                'query_builder' => function (CompetitionRepository $userRepository) use ($user) {
                    // call a method on your repository that returns the query builder
                    return $userRepository->getCompetitions($user);
                },
            ];

            $form->add('competition', EntityType::class, $formOptions);
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Competitor::class,
        ]);
    }
}
