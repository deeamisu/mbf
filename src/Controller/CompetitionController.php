<?php

namespace App\Controller;

use App\Entity\Competition;
use App\Form\CompetitionType;
use App\Repository\CompetitionRepository;
use App\Repository\EventRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @IsGranted("ROLE_SCRUTINEER")
 * @Route("scrut/competition")
 */
class CompetitionController extends AbstractController
{
    /**
     * @Route("/{id}", name="competition_index", methods={"GET"})
     */
    public function index( $id , EventRepository $eventRepository ): Response
    {
        $event=$eventRepository->find($id);

        return $this->render('competition/index.html.twig', [
            'competitions' => $event->getCompetitions(),
            'event' => $event,
        ]);
    }

    /**
     * @Route("/new/{id}", name="competition_new", methods={"GET","POST"})
     */
    public function new(Request $request, $id , EventRepository $eventRepository): Response
    {
        $competition = new Competition();
        $form = $this->createForm(CompetitionType::class, $competition);
        $form->handleRequest($request);

        $event=$eventRepository->find($id);

        if ($form->isSubmitted() && $form->isValid()) {


            $competition=$form->getData();
            $competition->setEvent($event);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($competition);
            $entityManager->flush();

            return $this->redirectToRoute('competition_index',['id'=>$id]);
        }

        return $this->render('competition/new.html.twig', [
            'competition' => $competition,
            'form' => $form->createView(),
            'event'=> $event
        ]);
    }

    /**
     * @Route("/show/{id}/{eventid}", name="competition_show", methods={"GET"})
     */
    public function show(Competition $competition, $eventid, EventRepository $eventRepository): Response
    {
        $event=$eventRepository->find($eventid);

        return $this->render('competition/show.html.twig', [
            'competition' => $competition,
            'event' => $event
        ]);
    }

    /**
     * @Route("/{id}/edit/{eventid}", name="competition_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Competition $competition , $eventid , EventRepository $eventRepository): Response
    {
        $form = $this->createForm(CompetitionType::class, $competition);
        $form->handleRequest($request);
        $event=$eventRepository->find($eventid);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('competition_index',['id'=>$eventid]);
        }

        return $this->render('competition/edit.html.twig', [
            'competition' => $competition,
            'form' => $form->createView(),
            'event' => $event
        ]);
    }

    /**
     * @Route("/{id}/{eventid}", name="competition_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Competition $competition , $eventid ,EventRepository $eventRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$competition->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($competition);
            $entityManager->flush();
        }

        $event=$eventRepository->find($eventid);

        return $this->redirectToRoute('competition_index',['id'=>$eventid, 'event'=>$event]);
    }

}
