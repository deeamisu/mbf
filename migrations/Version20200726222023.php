<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200726222023 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE criteria_competition DROP FOREIGN KEY FK_738A8A2990BEA15');
        $this->addSql('DROP TABLE criteria');
        $this->addSql('DROP TABLE criteria_competition');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE criteria (id INT AUTO_INCREMENT NOT NULL, president_id INT NOT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, procent INT NOT NULL, INDEX IDX_B61F9B81B40A33C7 (president_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE criteria_competition (criteria_id INT NOT NULL, competition_id INT NOT NULL, INDEX IDX_738A8A2990BEA15 (criteria_id), INDEX IDX_738A8A27B39D312 (competition_id), PRIMARY KEY(criteria_id, competition_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE criteria ADD CONSTRAINT FK_B61F9B81B40A33C7 FOREIGN KEY (president_id) REFERENCES president (id)');
        $this->addSql('ALTER TABLE criteria_competition ADD CONSTRAINT FK_738A8A27B39D312 FOREIGN KEY (competition_id) REFERENCES competition (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE criteria_competition ADD CONSTRAINT FK_738A8A2990BEA15 FOREIGN KEY (criteria_id) REFERENCES criteria (id) ON DELETE CASCADE');
    }
}
