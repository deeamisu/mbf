<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200728023434 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE scrutineer ADD active_event_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE scrutineer ADD CONSTRAINT FK_FCCADE0F8B7B9954 FOREIGN KEY (active_event_id) REFERENCES event (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FCCADE0F8B7B9954 ON scrutineer (active_event_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE scrutineer DROP FOREIGN KEY FK_FCCADE0F8B7B9954');
        $this->addSql('DROP INDEX UNIQ_FCCADE0F8B7B9954 ON scrutineer');
        $this->addSql('ALTER TABLE scrutineer DROP active_event_id');
    }
}
