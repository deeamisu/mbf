<?php
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 8/4/2020
 * Time: 9:49 PM
 */

namespace App\Service;


use App\Entity\Competitor;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;


abstract class Base
{
    protected $entityManager;

    private static $manager;

    private static $parameterBag;

    /**
     * Base constructor.
     * @param $entityManager
     * @param $parameterBag
     */
    public function __construct(EntityManagerInterface $entityManager, ParameterBagInterface $parameterBag)
    {
        $this->entityManager = $entityManager;
        self::$manager = $entityManager;
        self::$parameterBag = $parameterBag;
    }

    public static function getAllCompetitors()
    {

        return self::$manager->getRepository(Competitor::class)->findAll();
    }

    public function getPublicDir()
    {
        return self::$parameterBag->get('kernel.project_dir').'/public';
    }
}