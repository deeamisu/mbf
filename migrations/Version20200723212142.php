<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200723212142 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE event ADD president_id INT DEFAULT NULL, ADD scrutineer_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA7B40A33C7 FOREIGN KEY (president_id) REFERENCES president (id)');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA791741CE FOREIGN KEY (scrutineer_id) REFERENCES scrutineer (id)');
        $this->addSql('CREATE INDEX IDX_3BAE0AA7B40A33C7 ON event (president_id)');
        $this->addSql('CREATE INDEX IDX_3BAE0AA791741CE ON event (scrutineer_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE event DROP FOREIGN KEY FK_3BAE0AA7B40A33C7');
        $this->addSql('ALTER TABLE event DROP FOREIGN KEY FK_3BAE0AA791741CE');
        $this->addSql('DROP INDEX IDX_3BAE0AA7B40A33C7 ON event');
        $this->addSql('DROP INDEX IDX_3BAE0AA791741CE ON event');
        $this->addSql('ALTER TABLE event DROP president_id, DROP scrutineer_id');
    }
}
