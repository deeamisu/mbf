<?php

namespace App\Controller;

use App\Entity\Competition;
use App\Entity\Event;
use App\Entity\Result;
use App\Entity\Scrutineer;
use App\Service\ImportCompetitors;
use App\Service\Results;
use App\Service\ScrutineerMenu;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\File\MimeType;



/**
 * @IsGranted("ROLE_SCRUTINEER")
 * @Route("/scrut")
 */
class ScrutineerMenuController extends AbstractController
{
    /**
     * @Route("/scrutineer/menu", name="scrutineer_menu")
     */
    public function index()
    {
        return $this->render('scrutineer_menu/index.html.twig', [
            'controller_name' => 'ScrutineerMenuController',
        ]);
    }

    /**
     * @Route("/events/{id}", name="scrutineer_events" ,methods={"GET"})
     */
    public function get_Events($id, ScrutineerMenu $scrutineerMenu)
    {
    return $this->render('scrutineer_menu/events.html.twig',['events'=>$scrutineerMenu->getEvents($id)]);
    }

    /**
     * @Route("/import", name="scrutineer_import", methods={"GET"})
     */
    public function importCompetitorsToCompetition(ImportCompetitors $importCompetitors): Response
    {
        $competitors=$importCompetitors->importCompetitorsFromCsv();
        return $this->render('scrutineer/competitors.html.twig',['competitors' => $competitors]);
    }

    /**
     * @Route("/clear/competitors", name="scrutineer_clear_competitors")
     */
    public function clearCompetitors(ImportCompetitors $importCompetitors)
    {
        $importCompetitors->clearCompetitors();
        return $this->redirectToRoute('scrutineer_menu');
    }

    /**
     * @Route("/set/active/event/{id}", name="scrutineer_set_active_event", methods={"GET"})
     */
    public function setActiveEvent($id, EntityManagerInterface $entityManager )
    {
        $event=$entityManager->getRepository(Event::class)->find($id);

        /**
         * @var $scrutineer Scrutineer
         */
        $scrutineer=$event->getScrutineer();
        $scrutineer->setActiveEvent($event);
        $scrutineer->setActivate($event->getId());
        $entityManager->flush();


        return $this->render('scrutineer_menu/selectedEventMenu.html.twig',['event'=>$event]);
    }

    /**
     * @Route("/logoff/{logoff}",name="scrutineer_logoff", methods={"GET"})
     */
    public function logoff($logoff, EntityManagerInterface $entityManager)
    {
        $scrutineer=$entityManager->getRepository(Scrutineer::class)->find($logoff);
        $scrutineer->setActiveEvent(null);
        $scrutineer->setActivate(0);
        $entityManager->flush();

        return $this->redirectToRoute('scrutineer_logout');
    }

    /**
     * @Route("/scrutineer/menu/competitions/result{eventid}", name="scrutineer_competitions_result", methods={"GET"})
     */
    public function showCompetitionsToBeRendered($eventid, EntityManagerInterface $entityManager)
    {
        $event=$entityManager->getRepository(Event::class)->find($eventid);
        $competitions=$event->getCompetitions();

        return $this->render('scrutineer_menu/competitions.html.twig',
            ['competitions' => $competitions]);
    }

    /**
     * @Route("/scrutineer/menu/competition/show/result/{compid}", name="scrutineer_show_result", methods={"GET"})
     */
    public function showResult($compid, ScrutineerMenu $scrutineerMenu, EntityManagerInterface $entityManager)
    {
        $competition=$entityManager->getRepository(Competition::class)->find($compid);
        $results=$scrutineerMenu->result($compid);

        return $this->render('scrutineer_menu/results.html.twig',
            ['results' => $results, 'competition' => $competition]);
    }

    /**
     * @Route("/scrutineer/menu/competition/show/result/pdf/{competitionid}", name="pdf_competition_result", methods={"GET"})
     */
    public function pdfCompetitionResult($competitionid, EntityManagerInterface $entityManager, ScrutineerMenu $scrutineerMenu)
    {
        $competition=$entityManager->getRepository(Competition::class)->find($competitionid);
        $results=$scrutineerMenu->result($competitionid);

        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);

        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('scrutineer_menu/pdfResults.html.twig',
            ['results' => $results, 'competition' => $competition]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        $filename=$competition->getName().'.pdf';

        // Output the generated PDF to Browser (force download)
        $dompdf->stream($filename, [
            "Attachment" => false
        ]);
    }

    /**
     * @Route("/scrutineer/regfile/{eventid}", name="scrutineer_registration_file", methods={"GET"})
     */
    public function downloadRegFile($eventid, ScrutineerMenu $scrutineerMenu )
    {

        $filepath=$scrutineerMenu->setRegisterFile();
        $response = new BinaryFileResponse($filepath);
        $response->headers->set('Content-Type', 'text/csv');
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'inscrieri.csv'
        );

        return $response;

    }
}
