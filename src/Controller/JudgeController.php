<?php

namespace App\Controller;

use App\Entity\Judge;
use App\Form\JudgeType;
use App\Repository\JudgeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @IsGranted("ROLE_PRESIDENT")
 * @Route("presid/judge")
 */
class JudgeController extends AbstractController
{
    /**
     * @Route("/", name="judge_index", methods={"GET"})
     */
    public function index(JudgeRepository $judgeRepository): Response
    {
        return $this->render('judge/index.html.twig', [
            'judges' => $judgeRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="judge_new", methods={"GET","POST"})
     */
    public function new(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $judge = new Judge();
        $form = $this->createForm(JudgeType::class, $judge);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $judge=$form->getData();
            $password=$passwordEncoder->encodePassword($judge,$judge->getPassword());
            $judge->setPassword($password);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($judge);
            $entityManager->flush();

            return $this->redirectToRoute('judge_index');
        }

        return $this->render('judge/new.html.twig', [
            'judge' => $judge,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="judge_show", methods={"GET"})
     */
    public function show(Judge $judge): Response
    {
        return $this->render('judge/show.html.twig', [
            'judge' => $judge,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="judge_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Judge $judge, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $form = $this->createForm(JudgeType::class, $judge);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $judge=$form->getData();
            $password=$passwordEncoder->encodePassword($judge,$judge->getPassword());
            $judge->setPassword($password);

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('judge_index');
        }

        return $this->render('judge/edit.html.twig', [
            'judge' => $judge,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="judge_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Judge $judge): Response
    {
        if ($this->isCsrfTokenValid('delete'.$judge->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($judge);
            $entityManager->flush();
        }

        return $this->redirectToRoute('judge_index');
    }
}
