<?php

namespace App\Controller;

use App\Entity\Competition;
use App\Entity\Event;
use App\Service\JudgeMenu;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @IsGranted("ROLE_JUDGE")
 * @Route("/jdge")
 */
class JudgeMenuController extends AbstractController
{
    /**
     * @Route("/judge/menu", name="judge_menu")
     */
    public function index(EntityManagerInterface $entityManager)
    {
        $events=$entityManager->getRepository(Event::class)->findAll();

        return $this->render('judge_menu/index.html.twig', [
            'events' => $events,
        ]);
    }

    /**
     * @Route("/judge/menu/competitions/{eventid}/{judgeid}", name="judge_menu_competitions", methods={"GET"})
     */
    public function showEventCompetitions($eventid, $judgeid, EntityManagerInterface $entityManager, JudgeMenu $judgeMenu)
    {
        //$event=$entityManager->getRepository(Event::class)->find($eventid);
        //$competitions=$event->getCompetitions();
        $competitions=$judgeMenu->getCompetitions($judgeid,$eventid);

        return $this->render('judge_menu/competitions.html.twig',
            ['competitions' => $competitions]);
    }

    /**
     * @Route("/judge/menu/competition/competitors/{competitionid}/{judgeid}", name="judge_menu_competitors", methods={"GET"})
     */
    public function showCompetitionCompetitors($competitionid,$judgeid, EntityManagerInterface $entityManager, JudgeMenu $judgeMenu)
    {
        $competition=$entityManager->getRepository(Competition::class)->find($competitionid);
        $competitors=$competition->getCompetitors();
        $competitors=$judgeMenu->getCompetitors($competitors,$judgeid);

        $judgeMenu->calculate($competitors,$judgeid,$competitionid);

        return $this->render('judge_menu/competitors.html.twig',
            ['competitors' => $competitors, 'competitionid' => $competitionid, 'competition' => $competition]);
    }

    /**
     * @Route("/judge/menu/competition/competitor/grade/{competitionid}/{judgeid}/{athletid}", name="judge_menu_grade", methods={"GET"})
     */
    public function setGrades($competitionid, $judgeid, $athletid, JudgeMenu $judgeMenu)
    {
        $credentials=$judgeMenu->getGradeRequirements($competitionid,$judgeid,$athletid);

        return $this->render('judge_menu/grade.html.twig',
            [
                'criterias' => $credentials['criterias'],
                'competitionid' => $competitionid,
                'athletid' => $athletid,
                'judgeid' => $judgeid,
            ]);
    }

    /**
     * @Route("/judge/menu/competition/competitor/grade/get/{compid}/{judgeid}/{athletid}", name="judge_menu_getGrades", methods={"POST","GET"})
     */
    public function getGrades(Request $request, $compid,$judgeid,$athletid, EntityManagerInterface $entityManager, JudgeMenu $judgeMenu): Response
    {
        $competition=$entityManager->getRepository(Competition::class)->find($compid);
        $criterias=$competition->getCriteria();

        $grades=[];
        foreach ($criterias as $criteria):
            $mark=[];
            $mark['criteriaid']=$criteria->getId();
            $mark['value']=floatval($_POST[$criteria->getId()]);
            $grades[]=$mark;
        endforeach;

        $judgeMenu->compileGrades($grades,$judgeid,$athletid);

        $athlets=$competition->getCompetitors();
        $competitors=$judgeMenu->getCompetitors($athlets,$judgeid);

        $judgeMenu->calculate($competitors,$judgeid,$compid);

        return $this->render('judge_menu/competitors.html.twig',['competitionid' => $compid,'competitors' => $competitors, 'competition' => $competition]);
    }
}
