<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200727211209 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE competition_criteria (competition_id INT NOT NULL, criteria_id INT NOT NULL, INDEX IDX_876EA0AE7B39D312 (competition_id), INDEX IDX_876EA0AE990BEA15 (criteria_id), PRIMARY KEY(competition_id, criteria_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE competition_judge (competition_id INT NOT NULL, judge_id INT NOT NULL, INDEX IDX_E24CF1C27B39D312 (competition_id), INDEX IDX_E24CF1C2B7D66194 (judge_id), PRIMARY KEY(competition_id, judge_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE competition_criteria ADD CONSTRAINT FK_876EA0AE7B39D312 FOREIGN KEY (competition_id) REFERENCES competition (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE competition_criteria ADD CONSTRAINT FK_876EA0AE990BEA15 FOREIGN KEY (criteria_id) REFERENCES criteria (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE competition_judge ADD CONSTRAINT FK_E24CF1C27B39D312 FOREIGN KEY (competition_id) REFERENCES competition (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE competition_judge ADD CONSTRAINT FK_E24CF1C2B7D66194 FOREIGN KEY (judge_id) REFERENCES judge (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE competition_criteria');
        $this->addSql('DROP TABLE competition_judge');
    }
}
