<?php

namespace App\Repository;

use App\Entity\Competition;
use App\Entity\Scrutineer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;


/**
 * @method Competition|null find($id, $lockMode = null, $lockVersion = null)
 * @method Competition|null findOneBy(array $criteria, array $orderBy = null)
 * @method Competition[]    findAll()
 * @method Competition[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompetitionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Competition::class);
    }

    // /**
    //  * @return Competition[] Returns an array of Competition objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Competition
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * @param $user Scrutineer
     * @return Competition[]
     */
    public function getCompetitions($user)
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT p
            FROM App\Entity\Scrutineer p
            WHERE p.activate > 0
            AND p.id = :userid'
        )->setParameter('userid',$user->getId())
        ;

        $result=$query->getResult();
        $scrutineer=$result[0];

        $event=$scrutineer->getActiveEvent();
        $id=$event->getId();

        $qb=$entityManager->createQueryBuilder();

        $qb->select('u')
            ->from('App\Entity\Competition', 'u')
            ->where('u.event = :index')
            ->setParameter('index', $id)
            ->orderBy('u.name', 'ASC');

        return  $qb;
    }
}
