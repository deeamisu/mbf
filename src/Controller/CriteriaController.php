<?php

namespace App\Controller;

use App\Entity\Criteria;
use App\Form\CriteriaType;
use App\Repository\CriteriaRepository;
use App\Service\PresidentMenu;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @IsGranted("ROLE_PRESIDENT")
 * @Route("/presid/criteria")
 */
class CriteriaController extends AbstractController
{
    /**
     * @Route("/{pid}/{eid}", name="criteria_index", methods={"GET"})
     */
    public function index(CriteriaRepository $criteriaRepository, $pid, $eid, PresidentMenu $presidentMenu): Response
    {
        $credentials=$presidentMenu->getCredentials($pid,$eid);
        return $this->render('criteria/index.html.twig', [
            'criterias' => $criteriaRepository->findAll(),
            'president' => $credentials['president'],
            'event' => $credentials['event']
        ]);
    }

    /**
     * @Route("/new/{pid}/{eid}", name="criteria_new", methods={"GET","POST"})
     */
    public function new(Request $request, $pid, $eid, PresidentMenu $presidentMenu): Response
    {
        $credentials=$presidentMenu->getCredentials($pid,$eid);
        $criterion = new Criteria();
        $form = $this->createForm(CriteriaType::class, $criterion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $criterion=$form->getData();
            $criterion->setPresident($credentials['president']);
            $criterion->setEvent($credentials['event']);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($criterion);
            $entityManager->flush();

            return $this->redirectToRoute('criteria_index',['pid'=>$pid, 'eid'=>$eid]);
        }

        return $this->render('criteria/new.html.twig', [
            'criterion' => $criterion,
            'form' => $form->createView(),
            'president' => $credentials['president'],
            'event' => $credentials['event']
        ]);
    }

    /**
     * @Route("/{id}/{presid}/{eveid}", name="criteria_show", methods={"GET"})
     */
    public function show(Criteria $criterion, $presid, $eveid, PresidentMenu $presidentMenu): Response
    {
        $credentials=$presidentMenu->getCredentials($presid,$eveid);

        return $this->render('criteria/show.html.twig', [
            'criterion' => $criterion,
            'president' => $credentials['president'],
            'event' => $credentials['event']
        ]);
    }

    /**
     * @Route("/{id}/edit/{preid}/{eveid}", name="criteria_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Criteria $criterion, $preid, $eveid, PresidentMenu $presidentMenu): Response
    {
        $credentials=$presidentMenu->getCredentials($preid,$eveid);

        $form = $this->createForm(CriteriaType::class, $criterion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('criteria_index',['pid'=>$preid,'eid'=>$eveid]);
        }

        return $this->render('criteria/edit.html.twig', [
            'criterion' => $criterion,
            'form' => $form->createView(),
            'president' => $credentials['president'],
            'event' => $credentials['event']
        ]);
    }

    /**
     * @Route("/{id}/{presid}/{eveid}", name="criteria_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Criteria $criterion, $presid, $eveid): Response
    {
        if ($this->isCsrfTokenValid('delete'.$criterion->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($criterion);
            $entityManager->flush();
        }

        return $this->redirectToRoute('criteria_index',['pid'=>$presid,'eid'=>$eveid]);
    }
}
