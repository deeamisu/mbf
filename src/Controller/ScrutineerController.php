<?php

namespace App\Controller;

use App\Entity\Scrutineer;
use App\Form\ScrutineerType;
use App\Repository\ScrutineerRepository;
use App\Service\ImportCompetitors;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @IsGranted("ROLE_ADMIN")
 * @Route("/adm/scrutineer")
 */
class ScrutineerController extends AbstractController
{
    /**
     * @Route("/", name="scrutineer_index", methods={"GET"})
     */
    public function index(ScrutineerRepository $scrutineerRepository): Response
    {
        return $this->render('scrutineer/index.html.twig', [
            'scrutineers' => $scrutineerRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="scrutineer_new", methods={"GET","POST"})
     */
    public function new(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $scrutineer = new Scrutineer();
        $form = $this->createForm(ScrutineerType::class, $scrutineer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $scrutineer=$form->getData();
            $password=$passwordEncoder->encodePassword($scrutineer,$scrutineer->getPassword());
            $scrutineer->setPassword($password);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($scrutineer);
            $entityManager->flush();

            return $this->redirectToRoute('scrutineer_index');
        }

        return $this->render('scrutineer/new.html.twig', [
            'scrutineer' => $scrutineer,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="scrutineer_show", methods={"GET"})
     */
    public function show(Scrutineer $scrutineer): Response
    {
        return $this->render('scrutineer/show.html.twig', [
            'scrutineer' => $scrutineer,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="scrutineer_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Scrutineer $scrutineer, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $form = $this->createForm(ScrutineerType::class, $scrutineer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $scrutineer=$form->getData();
            $password=$passwordEncoder->encodePassword($scrutineer,$scrutineer->getPassword());
            $scrutineer->setPassword($password);

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('scrutineer_index');
        }

        return $this->render('scrutineer/edit.html.twig', [
            'scrutineer' => $scrutineer,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="scrutineer_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Scrutineer $scrutineer): Response
    {
        if ($this->isCsrfTokenValid('delete'.$scrutineer->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($scrutineer);
            $entityManager->flush();
        }

        return $this->redirectToRoute('scrutineer_index');
    }

}
