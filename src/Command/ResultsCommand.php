<?php

namespace App\Command;

use App\Service\Results;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ResultsCommand extends Command
{
    protected static $defaultName = 'app:results';

    private $results;

    /**
     * ResultsCommand constructor.
     * @param $results
     */
    public function __construct(Results $results)
    {
        parent::__construct();
        $this->results = $results;
    }


    protected function configure()
    {
        $this
            ->setDescription('Creates Pdf files containing each competitor results')
            //->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('email', null, InputOption::VALUE_NONE, 'Send email to competitor with created Pdf files')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        //$arg1 = $input->getArgument('arg1');

        /**if ($arg1) {
            $io->note(sprintf('You passed an argument: %s', $arg1));
        }*/

        $this->results->createPdfResultsFiles();

        $io->success('Fisierele cu rezultate au fost create.');

        if ($input->getOption('email') ) {
            // ...
            $this->results->sendEmail();

            $io->success('Mailurile au fost trimise.');
        }

        return 0;
    }
}
