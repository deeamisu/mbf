<?php
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 7/27/2020
 * Time: 12:17 AM
 */

namespace App\Service;

use App\Entity\Competition;
use App\Entity\Criteria;
use App\Entity\Event;
use App\Entity\Judge;
use App\Entity\President;
use Doctrine\ORM\EntityManagerInterface;

class PresidentMenu extends Base
{
    /**
    private $entityManager;

    /**
     * PresidentMenu constructor.
     * @param $entityManager
     */
    /**public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }*/

    public function getCredentials($pid,$eid)
    {
        $credentials=[];
        $chairperson=$this->entityManager->getRepository(President::class)->find($pid);
        $event=$this->entityManager->getRepository(Event::class)->find($eid);
        $credentials['president']=$chairperson;
        $credentials['event']=$event;
        return $credentials;
    }

    public function getCredentialsCompetitionPresident($pid,$cid,$addJudge=false,$addCriteria=false)
    {
        $credentials=[];

        $chairperson=$this->entityManager->getRepository(President::class)->find($pid);
        $competition=$this->entityManager->getRepository(Competition::class)->find($cid);



        if ($addJudge == true):
            $judges=$competition->getJudge();
            $judgesToBeAdded=$this->entityManager->getRepository(Judge::class)->findAll();
        foreach ($judges as $value):
            foreach ($judgesToBeAdded as $key=>$item)
            {
                if ($item->getId() == $value->getId()):
                    unset($judgesToBeAdded[$key]);
                endif;
            }
        endforeach;
            $credentials['judges']=$judges;
            $credentials['addbleJudges']=$judgesToBeAdded;
        endif;

        if ($addCriteria == true):
            $criterias=$competition->getCriteria();
            $criteriasToBeAdded=$this->entityManager->getRepository(Criteria::class)->findAll();
        foreach ($criterias as $value):
            foreach ($criteriasToBeAdded as $key=>$item)
            {
                if ($item->getId() == $value->getId()):
                    unset($criteriasToBeAdded[$key]);
                endif;
            }
        endforeach;
            $credentials['criterias']=$criterias;
            $credentials['addbleCriterias']=$criteriasToBeAdded;
        endif;


        $credentials['president']=$chairperson;
        $credentials['competition']=$competition;
        return $credentials;
    }
}