<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200728220554 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE grade (id INT AUTO_INCREMENT NOT NULL, competitor_id INT DEFAULT NULL, criteria_id INT DEFAULT NULL, judge_id INT DEFAULT NULL, value SMALLINT DEFAULT NULL, INDEX IDX_595AAE3478A5D405 (competitor_id), INDEX IDX_595AAE34990BEA15 (criteria_id), INDEX IDX_595AAE34B7D66194 (judge_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE grade ADD CONSTRAINT FK_595AAE3478A5D405 FOREIGN KEY (competitor_id) REFERENCES competitor (id)');
        $this->addSql('ALTER TABLE grade ADD CONSTRAINT FK_595AAE34990BEA15 FOREIGN KEY (criteria_id) REFERENCES criteria (id)');
        $this->addSql('ALTER TABLE grade ADD CONSTRAINT FK_595AAE34B7D66194 FOREIGN KEY (judge_id) REFERENCES judge (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE grade');
    }
}
