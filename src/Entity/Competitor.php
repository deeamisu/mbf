<?php

namespace App\Entity;

use App\Repository\CompetitorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CompetitorRepository::class)
 */
class Competitor
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $code;

    /**
     * @ORM\ManyToOne(targetEntity=Competition::class, inversedBy="competitors")
     */
    private $competition;

    /**
     * @ORM\OneToMany(targetEntity=Grade::class, mappedBy="competitor")
     */
    private $grades;

    /**
     * @ORM\OneToMany(targetEntity=PartialResult::class, mappedBy="competitor", orphanRemoval=true)
     */
    private $partialResults;

    /**
     * @ORM\OneToOne(targetEntity=Result::class, mappedBy="competitor", cascade={"persist", "remove"})
     */
    private $result;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name1;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name2;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    public function __construct()
    {
        $this->grades = new ArrayCollection();
        $this->partialResults = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getCompetition(): ?Competition
    {
        return $this->competition;
    }

    public function setCompetition(?Competition $competition): self
    {
        $this->competition = $competition;

        return $this;
    }

    function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->getName1().' '.$this->getName2();
    }

    /**
     * @return Collection|Grade[]
     */
    public function getGrades(): Collection
    {
        return $this->grades;
    }

    public function addGrade(Grade $grade): self
    {
        if (!$this->grades->contains($grade)) {
            $this->grades[] = $grade;
            $grade->setCompetitor($this);
        }

        return $this;
    }

    public function removeGrade(Grade $grade): self
    {
        if ($this->grades->contains($grade)) {
            $this->grades->removeElement($grade);
            // set the owning side to null (unless already changed)
            if ($grade->getCompetitor() === $this) {
                $grade->setCompetitor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PartialResult[]
     */
    public function getPartialResults(): Collection
    {
        return $this->partialResults;
    }

    public function addPartialResult(PartialResult $partialResult): self
    {
        if (!$this->partialResults->contains($partialResult)) {
            $this->partialResults[] = $partialResult;
            $partialResult->setCompetitor($this);
        }

        return $this;
    }

    public function removePartialResult(PartialResult $partialResult): self
    {
        if ($this->partialResults->contains($partialResult)) {
            $this->partialResults->removeElement($partialResult);
            // set the owning side to null (unless already changed)
            if ($partialResult->getCompetitor() === $this) {
                $partialResult->setCompetitor(null);
            }
        }

        return $this;
    }

    public function getResult(): ?Result
    {
        return $this->result;
    }

    public function setResult(Result $result): self
    {
        $this->result = $result;

        // set the owning side of the relation if necessary
        if ($result->getCompetitor() !== $this) {
            $result->setCompetitor($this);
        }

        return $this;
    }

    public function getName1(): ?string
    {
        return $this->name1;
    }

    public function setName1(string $name1): self
    {
        $this->name1 = $name1;

        return $this;
    }

    public function getName2(): ?string
    {
        return $this->name2;
    }

    public function setName2(string $name2): self
    {
        $this->name2 = $name2;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }



}
